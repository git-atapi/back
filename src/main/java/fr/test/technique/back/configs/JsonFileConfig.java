package fr.test.technique.back.configs;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;


/**
 * i use this configuration to mock data from a json file instead of a database ...
 */
@Getter
@Configuration
public class JsonFileConfig {

    @Value("classpath:products.json")
    private Resource jsonFileResource;
}
