package fr.test.technique.back.services;
import fr.test.technique.back.models.Product;
import fr.test.technique.back.repositories.JsonProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {



    @Autowired
    private JsonProductRepository jsonProductRepository;


    /**
     * get all products from json file
     * @return .
     */
    public List<Product> getAllProducts() {
        return jsonProductRepository.findAll();
    }

    /**
     * get a product by id from json file
     * @param id .
     * @return .
     */
    public Optional<Product> getProductById(Long id){
        return jsonProductRepository.findById(id);
    }


    /**
     * save a product in json file
     * @param product .
     * @return .*
     */
    public Product createProduct(Product product) {
        return jsonProductRepository.save(product);
    }

    /**
     * update a product in json file
     * @param id .
     * @param product .
     * @return .
     */
    public Product updateProduct(Long id, Product product) {
        product.setId(id);
        return jsonProductRepository.save(product);
    }


    /**
     * delete a product from json file
     * @param id .
     */
    public void deleteProduct(Long id) {
        jsonProductRepository.deleteById(id);
    }
}
