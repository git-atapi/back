package fr.test.technique.back.exceptions;


public class JsonFileNotFoundException extends RuntimeException {

    public JsonFileNotFoundException(String message) {
        super(message);
    }

}
