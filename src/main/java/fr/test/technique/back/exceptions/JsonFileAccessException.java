package fr.test.technique.back.exceptions;

public class JsonFileAccessException extends RuntimeException {

    public JsonFileAccessException(String message) {
        super(message);
    }

    public JsonFileAccessException(String message, Throwable cause) {
        super(message, cause);
    }


}
