package fr.test.technique.back.controllers;

import fr.test.technique.back.models.Product;
import fr.test.technique.back.services.ProductService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;


    /**
     * get all products end point
     * @return .
     */
    @GetMapping
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }


    /**
     * get a product by id end point
     * @param id .
     * @return .
     */
    @GetMapping("/{id}")
    public Product getProductById(@PathVariable Long id) {
        return productService.getProductById(id).orElseThrow();
    }


    /**
     * create a product end point
     * @param product .
     * @return .
     */
    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    /**
     * update a product end point
     * @param id .
     * @param product .
     * @return .
     */
    @PatchMapping("/{id}")
    public Product updateProduct(@PathVariable Long id, @RequestBody Product product) {
        return productService.updateProduct(id, product);
    }


    /**
     * delete a product end point
     * @param id .
     */
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }
}