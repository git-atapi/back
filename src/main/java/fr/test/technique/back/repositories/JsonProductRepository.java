package fr.test.technique.back.repositories;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.test.technique.back.configs.JsonFileConfig;
import fr.test.technique.back.exceptions.JsonFileAccessException;
import fr.test.technique.back.exceptions.JsonFileNotFoundException;
import fr.test.technique.back.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * i mocked serveral functions from CrudRepository interface to use them with a json file instead of a jpa  ...
 */
@Repository
public class JsonProductRepository {


    private final Resource jsonFileResource;
    private final ObjectMapper objectMapper;

    /**
     * constructor
     * @param jsonFileConfig .
     * @param objectMapper .
     */
    @Autowired
    public JsonProductRepository(JsonFileConfig jsonFileConfig, ObjectMapper objectMapper) {
        this.jsonFileResource = jsonFileConfig.getJsonFileResource();
        this.objectMapper = objectMapper;
        this.objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    // =======================*********************************************======================= //
    // =======================****************utils methods****************======================= //
    // =======================*********************************************======================= //
    // those method i shd move them to a utils class...


    /**
     * refacto reading from json file method and exception hundling ..
     * @return ...
     */
    private List<Product> readProductsFromFile() {
        try {
            if (!jsonFileResource.exists()) {
                throw new JsonFileNotFoundException("JSON file not found at " + jsonFileResource.getFilename());
            }
            JsonNode rootNode = objectMapper.readTree(jsonFileResource.getInputStream());
            JsonNode dataNode = rootNode.path("data");
            if (dataNode.isMissingNode()) {
                throw new JsonFileAccessException("Missing 'data' array in JSON file");
            }
            return objectMapper.convertValue(dataNode, new TypeReference<List<Product>>() {});
        } catch (IOException e) {
            throw new JsonFileAccessException("Error accessing JSON file", e);
        }
    }


    /**
     * write to file method : write a list of products to json file
     * @param products .
     * @throws IOException .
     */
    private void writeToFile(List<Product> products) throws IOException {
        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode.set("data", objectMapper.valueToTree(products));
        objectMapper.writeValue(jsonFileResource.getFile(), rootNode);
    }


    /**
     * get next id method : get the next id to use for a new product
     * @param products
     * @return
     */
    private Long getNextId(List<Product> products) {
        /** à optimiser juste pour faire passer les tests ...
         Long maxId = 0L;
         for (Product product : products) {
         if (product.getId() != null && product.getId() > maxId) {
         maxId = product.getId();
         }
         }
         return maxId + 1;
         */

        return products.stream()
                .map(Product::getId)
                .max(Comparator.naturalOrder())
                .orElse(0L) + 1;
    }






    /**
     * find all method : get all products from json file
     * @return .
     */
    public List<Product> findAll() {
        return readProductsFromFile();
    }



    /**
     * find by id method : get a product by id from json file
     * @param id .
     * @return .
     */
    public Optional<Product> findById(Long id) {
        return readProductsFromFile().stream()
                .filter(product -> product.getId().equals(id))
                .findFirst();
    }


    /**
     * save method : save a product to json file
     * @param product .
     * @return .
     */
    public Product save(Product product) {
        List<Product> products = readProductsFromFile(); // Using the centralized method
        // if the product doesn't have an ID, generate one and save new item
        if (product.getId() == null) {
            product.setId(getNextId(products));
        } else {
            // if the product has an ID, remove the old item from the list
            // to update wuth the new one
            products.removeIf(p -> p.getId().equals(product.getId()));
        }
        products.add(product);
        try {
            writeToFile(products);
        } catch (IOException e) {
            throw new JsonFileAccessException("Error writing to JSON file", e);
        }
        return product;
    }

    /**
     * delete by id method : delete a product by id from json file
     * @param id .
     */
    public void deleteById(Long id){
        // Use the centralized method for reading the file
        List<Product> products = readProductsFromFile();

        // Remove the product with the given ID
        boolean isRemoved = products.removeIf(p -> p.getId().equals(id));

        // Only write back to the file if a product was actually removed
        if (isRemoved) {
            try {
                // Write the updated list back to the file
                writeToFile(products);
            } catch (IOException e) {
                throw new JsonFileAccessException("Error writing to JSON file", e);
            }
        }
    }

}
