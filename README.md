- in case you want to test the application outside the developpement it's running on
- https://test-technique.atapi.fr

### i didn't get time to fix cors issues with swagger,
- all tests are working on - https://test-technique.atapi.fr/products with postman the collection is imported
- `https://test-technique.atapi.fr/swagger-ui/index.html`
# Spring Boot Test technique - Product Management

### Single Module Architecture

- I structured the project as a single module for simplicity and ease of management.

### Packages

#### Model Package

- Contains the `Product` models used in the application requested.

#### Repository Package

- Mocks data retrieval from a database.
- Retrieves data from a JSON file, simulating database interactions.

### Data Transfer Object (DTO)

- i know that a DTO for the `Product` entity would be typical in a standard application.  but i Skipped implementing DTOs and mappers for simplicity, as this is a basic test.

### Controller

- As requested, i have  Implemented a RESTful endpoints for product management (`GET`, `POST`, `PATCH`, `DELETE`).

### Exception Handling

- I Used exception handling to manage errors.
- i have made Some Custom responses are returned for different types of exceptions.

### Utility Methods

- Utility methods are used for various functionalities like reading and writing JSON data, handling errors, and formatting responses.
- those methode are mainly inside the reposotory class, best practice is to put them in a utility class.

### Key Features Implemented

#### JSON File-Based Repository (`JsonProductRepository`)

- Manages CRUD operations using a JSON file.
- Custom methods for reading (`readProductsFromFile`) and writing (`writeToFile`) JSON data.

#### Exception Management

- Custom exceptions such as `JsonFileAccessException` are defined for specific error scenarios.
- i have centralized the Exception handling in `ExceptionHandler` as `ControllerAdvice`.

#### Swagger Integration

- Swagger is integrated for API documentation and testing.
- Accessible at: `http://localhost:8885/swagger-ui/index.html`.

#### Configuration

- `JsonFileConfig` class to manage configuration related to the JSON file.

#### Refactoring for Efficiency

- Centralized file operation logic in `JsonProductRepository` to reduce code duplication and improve maintainability.

#### ID Management in Repository

- Manual ID generation logic for new entities in the absence of a database.


#### Runing the application
clone the project on the main folder just do a `mvn clean install` and then run the application as a spring boot application.
- i have added the jar file to the repo, you can run it with `java -jar back-0.0.1-SNAPSHOT.jar`
- Java 17 is required to run the jar file.



