FROM openjdk:17-alpine

ADD target/back-0.0.1-SNAPSHOT.jar back.jar

ENTRYPOINT ["java","-jar","back.jar"]

